const Product = require('./Product');
const Cart = require('./Cart');
const User = require('./User');
const Order = require('./Order');
const Admin = require('./Admin');
const Token = require('./Token');

module.exports = {
  Product,
  Cart,
  User,
  Admin,
  Order,
  Token,
};
